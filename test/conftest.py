import pytest
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as firefox_options


@pytest.fixture(scope="class")
def get_firefox_options():
    options = firefox_options()
    options.add_argument('firefox')
    options.add_argument('--start-maximized')
    return options


@pytest.fixture(scope="class")
def get_webdriver(get_firefox_options):
    options = get_firefox_options
    driver = webdriver.Firefox(options=options)
    return driver


@pytest.fixture(scope="class")
def setup(request, get_webdriver):
    driver = get_webdriver
    driver.implicitly_wait(11)
    url = 'https://phptravels.com/demo/'
    if request.cls is not None:
        request.cls.driver = driver
    driver.get(url)
    yield driver
    driver.quit()
