import pytest
from faker import Faker
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from time import sleep
from random import randint


@pytest.mark.usefixtures('setup')
class TestFill:

    @pytest.fixture(scope="class")
    def create_page(self):
        browser = self.driver
        browser.implicitly_wait(10)
        wait = WebDriverWait(browser, 10)
        wait.until(
            EC.element_to_be_clickable((
                By.CSS_SELECTOR,
                ".row>.wow:nth-child(4)>.resource-box>.row>.col-md-8>.col-md-12.row>.row>.form-group>div>a"))).click()
        browser.switch_to.window(browser.window_handles[1])
        elem = wait.until(EC.element_to_be_clickable((By.NAME, "email")))
        elem.send_keys('admin@phptravels.com')
        elem = browser.find_element(By.NAME, "password")
        elem.send_keys('demoadmin')
        wait.until(
            EC.element_to_be_clickable((
                By.CSS_SELECTOR,
                "form>button"))).click()
        browser.get('https://phptravels.net/api/admin/cms')
        wait.until(
            EC.invisibility_of_element_located((
                By.CSS_SELECTOR,
                "div[class='bodyload']")))
        browser.find_element(
            By.CSS_SELECTOR,
            "form.add_button>button").click()

        faker = Faker('ru_RU')
        elem = wait.until(EC.element_to_be_clickable((By.NAME, "pagetitle")))
        page_title = faker.sentence(nb_words=8)
        elem.send_keys(page_title)

        wait.until(
            EC.element_to_be_clickable((
                By.CSS_SELECTOR,
                "a[class*='cke_button cke_button__source cke_button_off']"))).click()
        elem = browser.find_element(
            By.CSS_SELECTOR,
            "textarea[dir='ltr']")

        text = faker.sentence(nb_words=100)
        elem.send_keys(text)

        kw = []
        for _ in range(randint(3, 10)):
            kw.append(faker.word())
        elem = browser.find_element(By.CSS_SELECTOR, "input[name='keywords']")
        elem.send_keys(', '.join(kw))

        elem = browser.find_element(By.CSS_SELECTOR, "input[name='pagedesc']")
        elem.send_keys(faker.sentence(nb_words=10))

        elem = Select(browser.find_element(
            By.CSS_SELECTOR,
            "select[name='status']"))
        elem.select_by_visible_text('Enable')
        elem = Select(browser.find_element(
            By.CSS_SELECTOR,
            "select[name='pagetarget']"))
        elem.select_by_visible_text('Self')

        faker = Faker('en_US')
        elem = browser.find_element(
            By.CSS_SELECTOR,
            "input[name='externalink']")
        elem.send_keys(f'https://{faker.domain_name(2)}')
        elem = browser.find_element(By.CSS_SELECTOR, "input[name='page_icon']")
        elem.send_keys(f'https://{faker.domain_name(2)}/{faker.word()}.ico')

        elem = browser.find_element(By.CSS_SELECTOR, "input[name='pageslug']")
        elem.clear()
        permalink = f'{faker.word()}-{faker.word()}'
        elem.send_keys(permalink)

        elem = browser.find_element(
            By.CSS_SELECTOR,
            "button[class*='btn btn-primary btn-block btn-lg']")
        elem.click()
        assert_data = {
            "permalink": permalink,
            "page_title": page_title,
            "text": text
        }
        return assert_data

    def test_page_is_created(self, create_page):
        browser = self.driver
        browser.get('https://phptravels.net/api/admin/cms')
        sleep(3)
        wait = WebDriverWait(browser, 8)
        wait.until(
            EC.presence_of_all_elements_located((
                By.CSS_SELECTOR,
                "tbody>.xcrud-row>td:nth-child(3)")))
        elems = browser.find_elements(
            By.CSS_SELECTOR, "tbody>.xcrud-row>td:nth-child(3)")
        elems_text = []
        for i in range(0, len(elems)):
            elems_text.append(elems[i].text)
        assert create_page['page_title'] in elems_text

    def test_page_title_is_correct(self, create_page):
        browser = self.driver
        permalink = create_page['permalink']
        browser.get(f'https://phptravels.net/api/{permalink}')
        wait = WebDriverWait(browser, 10)
        elem = wait.until(
            EC.visibility_of_element_located((
                By.CSS_SELECTOR,
                "h2[class=sec__title_list]")))
        assert create_page['page_title'] == elem.text

    def test_page_text_is_correct(self, create_page):
        browser = self.driver
        permalink = create_page['permalink']
        browser.get(f'https://phptravels.net/api/{permalink}')
        wait = WebDriverWait(browser, 10)
        elem = wait.until(
            EC.visibility_of_element_located((
                By.CSS_SELECTOR,
                "div[class='collapse-inner terms-inner']")))
        assert create_page['text'] == elem.text
